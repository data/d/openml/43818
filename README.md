# OpenML dataset: Buenos-Aires-Airbnb-Data

https://www.openml.org/d/43818

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
For Past a decade, Airbnb has emerged as a great personalized staying option for customers worldwide.This dataset gives the details of Airbnb listings in Buenos Aires as on 24th November 2019
Content
This dataset includes all information  about hosts, geographical availability, necessary metrics to make predictions and perform analysis
Acknowledgements
This public dataset was published by Airbnb and the exact source is found here
Inspiration
What can we know about various hosts?
What are the major busy areas of Buenos Aires?
Which hosts are one of the busiest and why ?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43818) of an [OpenML dataset](https://www.openml.org/d/43818). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43818/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43818/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43818/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

